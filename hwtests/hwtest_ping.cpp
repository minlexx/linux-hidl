#include <android/hidl/manager/1.0/IServiceManager.h>

using namespace android::hidl::manager::V1_0;

int main() {
	android::sp<IServiceManager> m = IServiceManager::getService();
	m->ping();
	return 1;
}
